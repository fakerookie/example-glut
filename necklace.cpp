/* This is an IFS for generating the "fractal necklace".
   It illustrates creation of vectors as difference of points,
   scaling of vectors, and translation of points by vectors
   Vector operations are defined using operator overloading.
   (Points and vectors are treated here as objects of the same type.) */

#include <math.h>
#include <GL/glut.h>

#define RAD 57.29577951
#define DegToRad(angle)	((angle)/RAD)

struct V2f	// declaration and initialization of the V2f structure: 2D vectors
{
    float x, y;
    V2f(float x1, float y1)
	{x=x1; y=y1;}
    V2f()
	{x=0; y=0;}
};

V2f operator+(V2f a, V2f b)		// vector addition
{
    return V2f(a.x+b.x, a.y+b.y);
}

V2f operator-(V2f a, V2f b)		// vector subtraction 
{
    return V2f(a.x-b.x, a.y-b.y);
}

V2f operator*(float c, V2f a)	// scalar multiplied by a vector
{
    return V2f(c*a.x, c*a.y);
}

/* Define the initial hexagon */

V2f A[6];

void Initialize()
{
    for (int i=0; i<6; i++)
	A[i] = V2f(cos(DegToRad(i*60)), sin(DegToRad(i*60)));
}

/* The IFS */

void Generate(V2f P, int i)
{
    glVertex2f(P.x, P.y);
    if (i>0) {
	for(int j=0; j<6; j++)
	    Generate(A[j]+0.33*(P-A[j]), i-1);
    }
    return;
}

/* Initialize OpenGL */

void init (void) 	
{
    glClearColor (0.0, 0.0, 0.0, 0.0);	// select clearing color

    glMatrixMode(GL_PROJECTION);			// initialize viewing
    glLoadIdentity();
    glOrtho(-1.2, 1.2, -1.2, 1.2, -1.0, 1.0);
    glColor3f (1.0, 1.0, 1.0);
}

/* Define object to be displayed */

void display(void)		
{
    glClear (GL_COLOR_BUFFER_BIT);	// clear all pixels

    glPointSize(5.0);
    glBegin(GL_POINTS);
    Initialize();
    Generate (V2f(1,0), 4);
    glEnd();
    glFlush();
}

/* 
 * Declare initial window size, position, and display mode
 * (single buffer and RGBA).  Open window with the program name
 * in its title bar.  Call initialization routines.
 * Register callback function to display graphics.
 * Enter main loop and process events.
 */
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize (750, 750); 
    glutInitWindowPosition (100, 100);
    glutCreateWindow (argv[1]);
    init ();
    glutDisplayFunc(display);
    glutMainLoop(); 
    return 0;   /* ANSI C requires main to return int. */
}
