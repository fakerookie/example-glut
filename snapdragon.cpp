/* This is an IFS for generating the dragon curve.
   It illustrates creation of vectors as difference of points,
   and 2D transformations (scaling, translation, and 2D rotations) in
   vector form.  Vector operations are defined using operator overloading.
   (Points and vectors are treated here as objects of the same type.) */

#include <math.h>
#include <GL/glut.h>

#define RAD 57.29577951
#define DegToRad(angle)	((angle)/RAD)

struct V2f	// declaration and initialization of the "Vector" structure
{
    float x, y;
    V2f(float x1, float y1)
	{x=x1; y=y1;}
    V2f()
	{x=0; y=0;}
};

V2f operator+(V2f a, V2f b)		// vector addition
{
    return V2f(a.x+b.x, a.y+b.y);
}

V2f operator-(V2f a, V2f b)		// vector subtraction
{
    return V2f(a.x-b.x, a.y-b.y);
}

V2f operator*(float c, V2f a)	// scalar multiplied by a vector
{
    return V2f(c*a.x, c*a.y);
}

V2f Rotate2(V2f a, float phi)	// rotation (as in matrix form)
{
    return (V2f(a.x*cos(phi)-a.y*sin(phi), a.x*sin(phi) + a.y*cos(phi)));
}

/* The IFS */

void Generate(V2f P, int i)
{
    glVertex2f(P.x, P.y);
    if (i>0) {
	V2f P1 = sqrt(2)/2 * Rotate2(P, DegToRad(45));
	V2f P2 = sqrt(2)/2 * Rotate2(P, DegToRad(135)) + V2f(1,0);
	Generate(P1, i-1);
	Generate(P2, i-1);
    }
    return;
}

/* Initialize OpenGL */

void init (void) 		
{
    glClearColor (0.0, 0.0, 0.0, 0.0);	// select clearing color

    glMatrixMode(GL_PROJECTION);			// initialoize viewing
    glLoadIdentity();
    glOrtho(-0.45, 1.35, -0.7, 1.1, -1.0, 1.0);
    glColor3f (1.0, 1.0, 1.0);
}

/* Define object to be displayed */

void display(void)		
{
    glClear (GL_COLOR_BUFFER_BIT);		// clear all pixels

    glPointSize(5.0);
    glBegin(GL_POINTS);
    Generate (V2f(1,0), 10);
    glEnd();
    glFlush();
}

/* 
 * Declare initial window size, position, and display mode
 * (single buffer and RGBA).  Open window with the program name
 * in its title bar.  Call initialization routines.
 * Register callback function to display graphics.
 * Enter main loop and process events.
 */
int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize (750, 750); 
    glutInitWindowPosition (100, 100);
    glutCreateWindow (argv[1]);
    init ();
    glutDisplayFunc(display);
    glutMainLoop(); 
    return 0;   /* ANSI C requires main to return int. */
}
